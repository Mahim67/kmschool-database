<?php


namespace Mahim\Query;

use PDO;
use Mahim\Db\Db;

class Pagination extends Db
{
    public function limit(){
        if (isset($_REQUEST['page'])){
            return $_REQUEST['page'];
        }
        else{
            return $page = 1;
        }
    }

    public function indexPaginator($page,$data){
        $start_data = ($page -1)*$data;

        $query = "select * from `kmschool` where `trash` = 0 limit $start_data,$data";
            $query = $this->dbh->prepare($query);
            $query->execute();
            return $query->fetchAll(PDO::FETCH_ASSOC);
    }
}