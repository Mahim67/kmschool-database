<?php

namespace Mahim\Query;

use Mahim\Db\Db;
use Mahim\Traits\Validation;
use PDO;

class Student extends Db
{
    use Validation;
    public function all_students_data()
    {
        $query = "SELECT * FROM `kmschool` where `trash` = 0";
        $query = $this->dbh->prepare($query);
        $query->execute();
        return $query->fetchAll(PDO::FETCH_ASSOC);

    }
    public function student_data_store($value)
    {
        $query = "INSERT INTO `kmschool` (`fullname`, `username`, `address`, `birthday`, `department`, `phone`, `email`, `password`,`image`,`trash`, `created_at`, `updated_at`) VALUES (:fullname,:username,:address,:birthday,:department,:phone,:email,:password,:image,0, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)";
        $query = $this->dbh->prepare($query);
        $query->bindParam('fullname',$value['fullname']);
        $query->bindParam('username',$value['username']);
        $query->bindParam('address',$value['address']);
        $query->bindParam('birthday',$value['birthday']);
        $query->bindParam('department',$value['department']);
        $query->bindParam('phone',$value['phone']);
        $query->bindParam('email',$value['email']);
        $query->bindParam('password',$value['password']);
        $query->bindParam('image',$value['image']);
        return $query->execute();
    }
    public function student_data_update($value)
    {
        $query = "update `kmschool` set `fullname` = :fullname,`username` = :username, `address` = :address,`birthday`=:birthday,`department` = :department, `phone`= :phone,`email` = :email, `password` = :password,`image` = :image, `updated_at`= current_timestamp where `id`= :id";
        $query = $this->dbh->prepare($query);
        $query->bindParam('fullname',$value['fullname']);
        $query->bindParam('username',$value['username']);
        $query->bindParam('address',$value['address']);
        $query->bindParam('birthday',$value['birthday']);
        $query->bindParam('department',$value['department']);
        $query->bindParam('phone',$value['phone']);
        $query->bindParam('email',$value['email']);
        $query->bindParam('password',$value['password']);
        $query->bindParam('image',$value['image']);
        $query->bindParam('id',$value['id']);
        return $query->execute();
    }

    public function single_view($id){
        $query = "select * from `kmschool` where `id` = :id";
        $query = $this->dbh->prepare($query);
        $query->bindParam('id',$id);
        $query->execute();
        return $query->fetch(PDO::FETCH_ASSOC);
    }

    public function delete($id){
        $query = "DELETE FROM `kmschool` WHERE `kmschool`.`id` = :id";
        $query = $this->dbh->prepare($query);
        $query->bindParam('id',$id);
        return $query->execute();
    }

    public function condition_change($id,$value){
        $query = "UPDATE `kmschool` SET `trash` = :trash WHERE `id` = :id";
        $query = $this->dbh->prepare($query);
        $query->bindParam('id', $id);
        $query->bindParam('trash', $value);
        return $query->execute();
    }

    public function trash_view(){
        $query = "SELECT * FROM `kmschool` where `trash` = 1";
        $query = $this->dbh->prepare($query);
        $query->execute();
        return $query->fetchAll(PDO::FETCH_ASSOC);
    }
}