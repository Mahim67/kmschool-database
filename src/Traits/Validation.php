<?php


namespace Mahim\Traits;


trait Validation
{
    public function basic_test($data){
        $data = htmlspecialchars($data);
        $data = trim($data);
        $data = stripcslashes($data);

        return $data;
    }
}
//trait ValidationDOB
//{
//    public function validDOB($date)
//    {
//        $minAge=strtotime("-18 YEAR");
//        $entrantAge= strtotime($date);
//
//        if ($entrantAge < $minAge)
//        {
//            return false;
//        }
//        return true;
//    }
//}