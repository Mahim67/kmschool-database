<?php


namespace Mahim\Db;

use PDO;
use PDOException;

class Db
{
    private $serverName = "localhost";
    private $dbName = "students";
    private $userName = "root";
    private $password = "";


    protected $dbh;

    public function __construct()
    {

        try {

            $this->dbh = new PDO("mysql:host=$this->serverName;dbname=$this->dbName", $this->userName, $this->password);

            $this->dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            echo "Connection Failed" . " " . $e->getMessage();
        }
    }
}