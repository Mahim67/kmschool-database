<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>K M School</title>
    <link rel="stylesheet" href="/Assets/css/style.css">
    <link rel="stylesheet" href="/Assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="/Assets/css/font-awesome.min.css">
</head>
<body>

<nav id="navbar">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header-left">
            <ul class="nav navbar-nav navbar-left">
                <li><a href="home.php"><img src="/Assets/image/school.png" alt=""></a></li>
                <li><a class="navbar-brand" href="home.php">K M Primary School</a></li>
            </ul>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="navbar-header-right">
            <ul class="nav navbar-nav navbar-right">
                <li class="active"><a href="home.php">Home</a></li>
                <li><a href="information.php">Informataion</a></li>
                <li><a href="student.php">Student</a></li>
                <li><a href="#">Contact Us</a></li>
                <li><a href="trash.php">Trash</a></li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>