<?php
include_once $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."vendor".DIRECTORY_SEPARATOR."autoload.php";
use Mahim\Query\Student;
$student = new Student();

$student_data = $student->single_view($_GET['id']);


include_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "views" . DIRECTORY_SEPARATOR . "elements" . DIRECTORY_SEPARATOR
    . "header.php" ?>

    <div class="container">
        <h1 style="color: black;font-weight: bold;text-align: center">Register your information</h1>
        <p style="color: #204d74;font-weight: bold;text-align: center">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse, illum.</p><hr>

        <div class="form" style="padding-top: 50px">
            <form class="form-horizontal" action="update.php" method="post">
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Full Name</label>
                    <div class="col-sm-6">
                        <input type="hidden" value="<?= $student_data['id']?>" name="id">

                        <input type="text" class="form-control" id="fullname" placeholder="Full Name" name="fullname" value="<?= $student_data['fullname']?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Username</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" id="username" placeholder="Username" name="username" value="<?= $student_data['username']?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Address</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" id="address" placeholder="Address" name="address" value="<?= $student_data['address']?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Date Of Birth</label>
                    <div class="col-sm-6">
                        <input type="date" class="form-control" id="DOB" placeholder="Date Of Birth" name="birthday" value="<?= $student_data['birthday']?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Department</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" id="department" placeholder="Department" name="department" value="<?= $student_data['department']?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Phone Number</label>
                    <div class="col-sm-6">
                        <input type="number" class="form-control" id="number" placeholder="Phone Number" name="phone" value="<?= $student_data['phone']?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
                    <div class="col-sm-6">
                        <input type="email" class="form-control" id="inputEmail3" placeholder="Email" name="email" value="<?= $student_data['email']?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputPassword3" class="col-sm-2 control-label">Password</label>
                    <div class="col-sm-6">
                        <input type="password" class="form-control" id="inputPassword3" placeholder="Password" name="password" value="<?= $student_data['password']?>">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox"> Remember me
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-default">Submit</button>
                    </div>
                </div>
            </form>

        </div>

    </div>
<?php
include_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "views" . DIRECTORY_SEPARATOR . "elements" . DIRECTORY_SEPARATOR
    . "footer.php" ?>