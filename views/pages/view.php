<?php
include_once $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."vendor".DIRECTORY_SEPARATOR."autoload.php";

include_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "views" . DIRECTORY_SEPARATOR . "elements" . DIRECTORY_SEPARATOR
. "header.php" ;


use Mahim\Query\Student;
$student = new Student();
$id = $_GET['id'];
$view = $student->single_view($id);

?>
<div class="container">
    <table class="table table-bordered" style="margin-top: 50px">
        <!-- On rows -->
        <tr class="active">
            <th>Type</th>
            <th>Value</th>
        </tr>

        <tr class="warning">
            <td>Fullname</td>
            <td><?=$view['fullname']?></td>
        </tr>
        <tr class="danger">
            <td>Username</td>
            <td><?=$view['username']?></td>
        </tr>
        <tr class="active">
            <td>Address</td>
            <td><?=$view['address']?></td>
        </tr>
        <tr class="success">
            <td>Date Of Birth</td>
            <td><?=$view['birthday']?></td>
        </tr>
        <tr class="info">
            <td>Department</td>
            <td><?=$view['department']?></td>
        </tr>
        <tr class="warning">
            <td>Phone Number</td>
            <td ><?=$view['phone']?></td>
        </tr>
        <tr class="danger">
            <td>E-mail</td>
            <td><?=$view['email']?></td>
        </tr>
        <tr class="active">
            <td>Password</td>
            <td><?=$view['password']?></td>
        </tr>
        <tr class="success">
            <td>Image</td>
            <td>
                <img src="data:image;base64,<?=$view['image']?>" alt="" width="150px" height="150px">
            </td>
        </tr>
    </table>

</div>


<?php

include_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "views" . DIRECTORY_SEPARATOR . "elements" . DIRECTORY_SEPARATOR
    . "footer.php" ?>

?>
