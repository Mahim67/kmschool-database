<?php
include_once $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."vendor".DIRECTORY_SEPARATOR."autoload.php";
use Mahim\Query\Student;
use Mahim\Query\Pagination;

$pagination = new Pagination();

$student = new Student();

$all_students = $student->all_students_data();

$total_data = count($all_students);

$total_pages = ceil($total_data/5);

$current_page = $pagination->limit();

$all_students = $pagination->indexPaginator($current_page,5);

include_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "views" . DIRECTORY_SEPARATOR . "elements" . DIRECTORY_SEPARATOR
. "header.php" ;




?>

<div class="container">
    <h1 style="color: black;font-weight: bold;text-align: center">All Students Information</h1>
    <p style="color: #204d74;font-weight: bold;text-align: center">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequatur, similique.</p><hr>

    <div class="table" style="padding-top: 30px">

        <table class="table table-bordered">


            <tr>
                <td class="success" style="font-weight: bold;color: black">Full Name</td>
                <td class="warning" style="font-weight: bold;color: black">UserName</td>
                <td class="success" style="font-weight: bold;color: black">Address</td>
                <td class="info" style="font-weight: bold;color: black">Email</td>
                <td class="info" style="font-weight: bold;color: black">Action</td>
            </tr>

            <?php
                           foreach ($all_students as $all_student):
                ?>

                <tr> 
                    <td class="success"><?php echo $all_student['fullname']?></td>
                    <td class="warning"><?php echo $all_student['username']?></td>
                    <td class="success"><?php echo $all_student['address']?></td>
                    <td class="info"><?php echo $all_student['email']?></td>
                    <td class="info"><a href="view.php?id=<?= $all_student['id']?>">View</a> || <a href="edit.php?id=<?= $all_student['id']?>">Edit</a> || <a href="soft_delete.php?id=<?= $all_student['id']?>" onclick="return confirm('are You sure to Delete this')">Delete</a></td>
                </tr>

                <?php
            endforeach;
            ?>
        </table>

        <?php

        $plusonepage = $current_page + 1;
        $minusonepage = $current_page - 1;

        if ($current_page >1)echo "<a href='information.php?page=$minusonepage'> Previus Page</a>";
        for ($i = 1;$i<=$total_pages;$i++): ?>

        <ul style="list-style: none">
            <li><a href="information.php?page=<?php echo $i ?>"><?php echo $i ?></a></li>
        </ul>
        <?php
        endfor;
        if ($current_page < 1)echo "<a href='information.php?page=$plusonepage'> Next Page</a>";
        ?>

    </div>

</div>

<?php
include_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "views" . DIRECTORY_SEPARATOR . "elements" . DIRECTORY_SEPARATOR
    . "footer.php" ?>
