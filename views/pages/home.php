<?php
include_once $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."views".DIRECTORY_SEPARATOR."elements".DIRECTORY_SEPARATOR."header.php"
?>
<!--carousel start-->

<div class="carousel">
    <div class="panel_carousel">
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                <li data-target="#carousel-example-generic" data-slide-to="2"></li>
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner">
                <div class="item active">
                    <img src="../../Assets/image/pic1.jpg" alt="">
                    <div class="carousel-caption">

                    </div>
                </div>
                <div class="item">
                    <img src="../../Assets/image/pic2.jpg" alt="">
                    <div class="carousel-caption">

                    </div>
                </div>
                <div class="item">
                    <img src="../../Assets/image/pic3.jpg" alt="">
                    <div class="carousel-caption">

                    </div>
                </div>
            </div>

            <!-- Controls -->
            <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            </a>
            <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            </a>
        </div>

    </div>


</div>

<?php
include_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "views" . DIRECTORY_SEPARATOR . "elements" . DIRECTORY_SEPARATOR
    . "footer.php" ?>