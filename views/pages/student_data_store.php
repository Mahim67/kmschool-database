<?php
include_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR . "autoload.php";

if (!isset($_SESSION)){
    session_start();
}
use Mahim\Query\Student;
$student = new Student();

$_POST['image'] = base64_encode(file_get_contents(addslashes($_FILES['image']['tmp_name'])));



/*fullname validation start*/
if (empty($_POST['fullname'])){
    $_SESSION['validation']['fullname'] = "<span style='color: red'>*Fullname is Required</span>";
}
else{
    if (preg_match('/^[a-zA-Z\s]+$/',$_POST['fullname'])){
        if (strlen((string)$_POST['fullname']) > 3 and strlen($_POST['fullname']) < 25){
            $_POST['fullname'] = $student->basic_test($_POST['fullname']);
        }else{
            $_SESSION['validation']['fullname'] = "<span style='color: red'>*Fullname MAX length 25 & MIN length 5</span>";
        }
    }
    else{
        $_SESSION['validation']['fullname'] = "<span style='color: red'>*Enter valid string</span>";
    }
}
/*fullname validation end*/


/*username validation strat*/
if (empty($_POST['username'])){
    $_SESSION['validation']['username'] = "<span style='color: red'>*Username is Required</span>";
}
else{
    if (preg_match('/^[A-Za-z_-]*$/',$_POST['username'])){
        if (strlen($_POST['username']) > 3 and strlen($_POST['username']) < 25){
            $_POST['username'] = $student->basic_test($_POST['username']);
        }else{
            $_SESSION['validation']['username'] = "<span style='color: red'>*Username MAX length 15 & MIN length 3</span>";
        }
    }
    else{
        $_SESSION['validation']['username'] = "<span style='color: red'>*Enter valid string</span>";
    }
}
/*username validation end*/
/*address validation strat*/
if (empty($_POST['address'])){
    $_SESSION['validation']['address'] = "<span style='color: red'>*Address is Required</span>";
}
else{
    if (strlen($_POST['address']) > 5 and strlen($_POST['address']) < 25){
        $_POST['address'] = $student->basic_test($_POST['address']);
    }else{
        $_SESSION['validation']['address'] = "<span style='color: red'>*Username MAX length 25 & MIN length 5</span>";
    }
}
/*address validation end*/
/*birthday validation start*/
if (empty($_POST['birthday'])){
    $_SESSION['validation']['birthday'] = "<span style='color: red'>*Birthday is Required</span>";
}
else{
    if (time($_POST['birthday']) > strtotime('+18 years',strtotime($_POST['birthday']))){
       $_POST['birthday'] = $student->basic_test($_POST['birthday']);
    }
    else{
        $_SESSION['validation']['birthday'] = "<span style='color: red'>*You are too young!</span>";
    }
}
/*birthday validation end*/
/*department validation start*/
if (empty($_POST['department'])){
    $_SESSION['validation']['department'] = "<span style='color: red'>*Department is Required</span>";
}
else{
    if (preg_match('/^[A-Za-z_-]*$/',$_POST['department'])){
        if (strlen($_POST['department']) > 4 and strlen($_POST['department']) < 15){
            $_POST['department'] = $student->basic_test($_POST['department']);
        }
        else{
            $_SESSION['validation']['department'] = "Max length 15 or Min length 4";
        }
    }
    else{
        $_SESSION['validation']['department'] = "enter valid charaters";
    }
}
/*department validation end*/
/*phone number validation start*/
//$error = [];
if (empty($_POST['phone'])){
    //$error['phone'] = "Phone Number is Requited";
    $_SESSION['validation']['phone'] = "<span style='color: red'>*Phone Number is Required</span>";
}
else{
    if (preg_match('/^[0-9]+$/',$_POST['phone'])){
        if (strlen((string)$_POST['phone']) > 10 and strlen((string)$_POST['phone']) < 15){
            $_POST['phone'] = $student->basic_test($_POST['phone']);
        }
        else{
            $_SESSION['validation']['phone'] = "Phone Number max lenght 15 and min length 10";
        }
    }
    else{
        $_SESSION['validation']['phone'] = "only number enter";
    }
}
/*phone number validation end*/
/*email validation strat*/
if (empty($_POST['email'])){
    $_SESSION['validation']['email'] = "<span style='color: red'>*Eamil feild is Required</span>";
}
else{
    if (filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
        $_POST['email'] = $student->basic_test($_POST['email']);
    }
    else{
        $_SESSION['validation']['email'] = "<span style='color: red'>*Invalid email format</span>";
    }
}
/*email validation end*/
/*password validation start*/
if (empty($_POST['password'])){
    $_SESSION['validation']['password'] = "<span style='color: red'>*Password is Required</span>";
}
else{
    if (preg_match('/^(?=.*\d)(?=.*[A-Za-z])[0-9A-Za-z!@#$%]{6,15}$/', $_POST['password'])) {
        $_POST['password'] = $student->basic_test($_POST['password']);
    }
    else{
        $_SESSION['validation']['password'] = "<span style='color: red'>*the password does not meet the requirements</span>";
    }
}
/*password validation end*/

if(empty($_SESSION['validation'])){
    $student->student_data_store($_POST);
    header('location:information.php');
}
else {
    header('location:student.php');
}


