<?php
include_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "views" . DIRECTORY_SEPARATOR . "elements" . DIRECTORY_SEPARATOR
    . "header.php" ?>
<?php
if (!isset($_SESSION)) {
    session_start();
}
?>


    <div class="container">
        <h1 style="color: black;font-weight: bold;text-align: center">Register your information</h1>
        <p style="color: #204d74;font-weight: bold;text-align: center">Lorem ipsum dolor sit amet, consectetur
            adipisicing elit. Esse, illum.</p>
        <hr>

        <div class="form" style="padding-top: 50px">
            <form class="form-horizontal" action="student_data_store.php" method="post" enctype="multipart/form-data">
                <div class="form-group">


                    <label for="" class="col-sm-2 control-label">Full Name</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" id="fullname" placeholder="Full Name" name="fullname">
                    </div>
                    <?php
                    if (isset($_SESSION['validation']['fullname'])){
                        echo $_SESSION['validation']['fullname'];
                        unset($_SESSION['validation']['fullname']);
                    }
                    ?>
                </div>

                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Username</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" id="username" placeholder="Username" name="username">
                    </div>
                    <?php
                    if (isset($_SESSION['validation']['username'])){
                        echo $_SESSION['validation']['username'];
                        unset($_SESSION['validation']['username']);
                    }
                    ?>
                </div>

                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Address</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" id="address" placeholder="Address" name="address">
                    </div>
                    <?php
                    if (isset($_SESSION['validation']['address'])){
                        echo $_SESSION['validation']['address'];
                        unset($_SESSION['validation']['address']);
                    }
                    ?>
                </div>

                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Date Of Birth</label>
                    <div class="col-sm-6">
                        <input type="date" class="form-control" id="DOB" placeholder="Date Of Birth" name="birthday">
                    </div>
                    <?php
                    if (isset($_SESSION['validation']['birthday'])){
                        echo $_SESSION['validation']['birthday'];
                        unset($_SESSION['validation']['birthday']);
                    }
                    ?>
                </div>

                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Department</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" id="department" placeholder="Department"
                               name="department">
                    </div>
                    <?php
                    if (isset($_SESSION['validation']['department'])){
                        echo $_SESSION['validation']['department'];
                        unset($_SESSION['validation']['department']);
                    }
                    ?>
                </div>

                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Phone Number</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" id="number" placeholder="Phone Number" name="phone">
                    </div>
                    <?php
                    if (isset($_SESSION['validation']['phone'])){
                        echo $_SESSION['validation']['phone'];
                        unset($_SESSION['validation']['phone']);
                    }
                    ?>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
                    <div class="col-sm-6">
                        <input type="email" class="form-control" id="inputEmail3" placeholder="Email" name="email">
                    </div>
                    <?php
                    if (isset($_SESSION['validation']['email'])){
                        echo $_SESSION['validation']['email'];
                        unset($_SESSION['validation']['email']);
                    }
                    ?>
                </div>
                <div class="form-group">
                    <label for="inputPassword3" class="col-sm-2 control-label">Password</label>
                    <div class="col-sm-6">
                        <input type="password" class="form-control" id="inputPassword3" placeholder="Password"
                               name="password">
                    </div>
                    <?php
                    if (isset($_SESSION['validation']['password'])){
                        echo $_SESSION['validation']['password'];
                        unset($_SESSION['validation']['password']);
                    }
                    ?>
                </div>

                <div class="form-group">
                    <label for="inputPassword3" class="col-sm-2 control-label">File Upload</label>
                    <div class="col-sm-6">
                        <input type="file" id="inputPassword3" name="image">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox"> Remember me
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-default">Submit</button>
                    </div>
                </div>
            </form>

        </div>

    </div>
<?php

include_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "views" . DIRECTORY_SEPARATOR . "elements" . DIRECTORY_SEPARATOR
    . "footer.php" ?>